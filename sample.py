# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 01:36:03 2023

@author: imran
"""

import os
import time

# Update the output_folder path to the GitLab project's root directory
output_folder = "./"
output_file_path = f"{output_folder}base1.txt"
print(f"Script{os.getcwd()}")

def main():
    with open(output_file_path, "w") as output_file:
        output_file.write("Hello, GitLab Runner!")

    # Sleep for 4 hours (14400 seconds)
    # time.sleep(14400)

    # Delete the output file after 4 hours
    #if os.path.exists(output_file_path):
    #    os.remove(output_file_path)

if __name__ == "__main__":
    main()
